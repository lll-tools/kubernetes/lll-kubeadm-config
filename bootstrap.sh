#!/usr/bin/env bash

# Fedora 33
/vagrant_data/bin/bootstrap-f33.sh

# HELM
curl -fsSL -o get_helm.sh \
     https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod u+x *.sh
VERIFY_CHECKSUM=false ./get_helm.sh
helm version

# SSH Keys
mkdir -p /home/vagrant/.ssh
cat /vagrant_data/etc/secrets/ssh_keys.pub >> /home/vagrant/.ssh/authorized_keys

# Kubeadm Control Plane
/vagrant_data/bin/bootstrap-kubeadm.sh
