#!/bin/bash

# UPDATE
dnf check-update
dnf install -y dnf-plugins-core wget curl openssl

# SWAP
systemctl status swap-create@zram0
systemctl stop swap-create@zram0
systemctl disable swap-create@zram0
systemctl status swap-create@zram0

ls -la /etc/systemd/zram-generator.conf
touch /etc/systemd/zram-generator.conf
ls -la /etc/systemd/zram-generator.conf

swapon --show
swapoff -a
swapon --show
sed -i.default -e 's/^\(.*[ \t]*swap\)/#\1/' /etc/fstab

# NETWORK
modprobe br_netfilter
sysctl net.ipv4.ip_forward=1
#sysctl net.bridge.bridge-nf-call-ip6tables=1
#sysctl net.bridge.bridge-nf-call-iptables=1
sysctl --system

#firewall-cmd --zone=public --permanent --add-port={6443,2379,2380,10250,10251,10252}/tcp
iptables -L
systemctl status firewalld
systemctl stop firewalld
systemctl disable firewalld
systemctl status firewalld
iptables -L

# SELINUX
#sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
getenforce
setenforce 0
getenforce

# DOCKER
dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
# dnf -y install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
dnf -y install docker-ce docker-ce-cli containerd.io
dnf list docker-ce --showduplicates | sort -r

systemctl status docker
systemctl start docker
systemctl enable docker
systemctl status docker

# KUBERNETES
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

dnf install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable --now kubelet
kubeadm version
