#!/bin/bash

CLUSTER_IP="${K8S_CLUSTER_IP:-192.168.56.10}"
CLUSTER_NAME="${K8S_CLUSTER_NAME:-k8s.luislain.com}"
DATA_DIR=/vagrant_data
CALICO_DIR=${DATA_DIR}/var/calico

# RESET ---------------------------
#kubeadm reset -f
#rm -Rvf /etc/cni/net.d
rm -v /etc/kubernetes/admin.conf
rm -v $HOME/.kube/config
# iptables-save > iptables.bak
# iptables -P INPUT ACCEPT
# iptables -P FORWARD ACCEPT
# iptables -P OUTPUT ACCEPT
# iptables -t nat -F
# iptables -t mangle -F
# iptables -F
# iptables -X
# ip6tables -P INPUT ACCEPT
# ip6tables -P FORWARD ACCEPT
# ip6tables -P OUTPUT ACCEPT
# ip6tables -t nat -F
# ip6tables -t mangle -F
# ip6tables -F
# ip6tables -X

# CONTROL PLANE --------------------
cat /sys/class/dmi/id/product_uuid
ip link

hostname
hostnamectl set-hostname ${CLUSTER_NAME}
hostname
echo "${CLUSTER_IP}	${CLUSTER_NAME}" >> /etc/hosts
cp -v /etc/hosts ${DATA_DIR}/hosts.${CLUSTER_NAME}

kubeadm init --control-plane-endpoint=${CLUSTER_NAME} \
	--apiserver-advertise-address=${CLUSTER_IP} \
	--pod-network-cidr 192.168.0.0/16 | tee ${DATA_DIR}/controlplane.${CLUSTER_NAME}
#	--native.cgroupdriver=systemd

# KUBECTL
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> /root/.profile
export KUBECONFIG=/etc/kubernetes/admin.conf
mkdir -p /$HOME/.kube
cp -v $KUBECONFIG $HOME/.kube/config
chown -Rv $(id -u):$(id -g) $HOME/.kube
cp -vf $KUBECONFIG "${DATA_DIR}/config.${CLUSTER_NAME}"
echo "kubectl --kubeconfig config.${CLUSTER_NAME} \$@" > "${DATA_DIR}/kubectl.${CLUSTER_NAME}.sh"
kubectl get pods -A

# CALICO
mkdir -p $CALICO_DIR && cd $CALICO_DIR
if [ ! -f calico-v1.yaml ]; then
    curl https://docs.projectcalico.org/manifests/custom-resources.yaml -o calico-custom-res.yaml
    curl https://docs.projectcalico.org/manifests/calico.yaml -o calico.yaml
    sed -i.default -e 's/policy\/v1beta1/policy\/v1/' calico.yaml
    mv calico.yaml calico-v1.yaml
fi
kubectl apply -f calico-v1.yaml
cd -

kubectl taint nodes --all node-role.kubernetes.io/master-

# INGRESS-NGINX
#curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.46.0/deploy/static/provider/baremetal/deploy.yaml -o ingress-nginx.yaml
# helm repo add nginx-stable https://helm.nginx.com/stable
# helm repo update
helm upgrade --install ingress-nginx ingress-nginx \
     --repo https://kubernetes.github.io/ingress-nginx \
     --namespace ingress-nginx --create-namespace \
     --set controller.setAsDefaultIngress=true \
     --set controller.service.loadBalancerIP="${CLUSTER_IP}" \
     --set controller.service.externalIPs={"${CLUSTER_IP}"} \
     --set prometheus.create=true
     # --set prometheus.port

# kubectl patch svc ingress-nginx-controller \
# 	-n ingress-nginx \
# 	-p '{"spec": {"type": "LoadBalancer", "externalIPs":["$CLUSTER_IP"]}}'

# METRICS
#https://artifacthub.io/packages/helm/metrics-server/metrics-server
#https://stackoverflow.com/questions/52694238/kubectl-top-node-error-metrics-not-available-yet-using-metrics-server-as-he
#kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.4.2/components.yaml
#helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm upgrade --install metrics-server metrics-server \
     --repo https://kubernetes-sigs.github.io/metrics-server \
     --namespace admin-tools --create-namespace

kubectl get pod -A
kubectl get svc -A
