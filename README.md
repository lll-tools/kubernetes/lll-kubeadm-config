- [Introduction](#orgacc6d7e)
- [Bootstrap Fedora 33](#org29f7591)
  - [Update](#org13940c9)
  - [Swap](#orgc6d7889)
  - [Network traffic](#orgb98e023)
  - [SELinux](#org782ef8c)
  - [dnf Install docker.io](#org591a7cd)
  - [dnf Install kubeadm, kubelet and kubectl](#org14f91a1)
- [Bootstrap K8s](#org3b13832)
  - [Install docker.io](#org072f0d7)
  - [Install kubeadm, kubelet and kubectl](#org0a72e50)
  - [Install helm](#org075ad6d)
- [Bootstrap Control plane (kubeadm+calico+ingress-nginx)](#orgd0e766f)
  - [Reset cluster](#org1dc67d6)
  - [Name cluster](#orgc8c8062)
  - [Init cluster](#org71341ea)
  - [Set-up kubectl](#org9a5d641)
  - [Deploy calico](#orgd16c3bb)
  - [Taint master node](#orgb0f8d17)
  - [Deploy ingress-nginx](#org5e90139)
  - [Deploy metrics-server](#org482fd2f)
- [Test Control plane](#org6989cd9)
  - [Modify hosts file](#orga8e149c)
  - [Copy kubeconfig file](#orga4b5506)
  - [List pods and services](#org97b21ae)
  - [Join control plane node](#orgdea118f)
  - [Join worker node](#org478f34d)


<a id="orgacc6d7e"></a>

# Introduction

Create a kubernetes cluster using kubeadm in Fedora 33. If you are using Virtualbox and Vagrant, execute:

```shell
vagrant box update
vagrant up --provision
vagrant ssh
vagrant suspend
vagrant resume
```


<a id="org29f7591"></a>

# Bootstrap Fedora 33


<a id="org13940c9"></a>

## Update

```shell
dnf check-update
dnf install -y dnf-plugins-core wget curl
```


<a id="orgc6d7889"></a>

## Swap

```shell
systemctl status swap-create@zram0
systemctl stop swap-create@zram0
systemctl disable swap-create@zram0
systemctl status swap-create@zram0

ls -la /etc/systemd/zram-generator.conf
touch /etc/systemd/zram-generator.conf
ls -la /etc/systemd/zram-generator.conf

swapon --show
swapoff -a
swapon --show

sed -i.default -e 's/^\(.*[ \t]*swap\)/#\1/' /etc/fstab
```


<a id="orgb98e023"></a>

## Network traffic

```shell
modprobe br_netfilter
sysctl net.ipv4.ip_forward=1
#sysctl net.bridge.bridge-nf-call-iptables=1
#sysctl net.bridge.bridge-nf-call-ip6tables=1
sysctl --system

#firewall-cmd --zone=public --permanent --add-port={6443,2379,2380,10250,10251,10252}/tcp
iptables -L
systemctl status firewalld
systemctl stop firewalld
systemctl disable firewalld
systemctl status firewalld
iptables -L
```


<a id="org782ef8c"></a>

## SELinux

```shell
#sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
getenforce
setenforce 0
getenforce
```


<a id="org591a7cd"></a>

## dnf Install docker.io

```shell
dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
dnf -y install docker-ce docker-ce-cli containerd.io
dnf list docker-ce --showduplicates | sort -r
systemctl status docker
systemctl start docker
systemctl enable docker
systemctl status docker
```


<a id="org14f91a1"></a>

## dnf Install kubeadm, kubelet and kubectl

```shell
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

dnf install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable --now kubelet
```


<a id="org3b13832"></a>

# Bootstrap K8s


<a id="org072f0d7"></a>

## Install docker.io


<a id="org0a72e50"></a>

## Install kubeadm, kubelet and kubectl


<a id="org075ad6d"></a>

## Install helm

```shell
curl -fsSL -o get_helm.sh \
     https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod u+x *.sh
VERIFY_CHECKSUM=false ./get_helm.sh
helm version
```


<a id="orgd0e766f"></a>

# Bootstrap Control plane (kubeadm+calico+ingress-nginx)


<a id="org1dc67d6"></a>

## Reset cluster

```shell
#kubeadm reset -f
#rm -Rvf /etc/cni/net.d
rm -v /etc/kubernetes/admin.conf
rm -v $HOME/.kube/config
# iptables-save > iptables.bak
# iptables -P INPUT ACCEPT
# iptables -P FORWARD ACCEPT
# iptables -P OUTPUT ACCEPT
# iptables -t nat -F
# iptables -t mangle -F
# iptables -F
# iptables -X
# ip6tables -P INPUT ACCEPT
# ip6tables -P FORWARD ACCEPT
# ip6tables -P OUTPUT ACCEPT
# ip6tables -t nat -F
# ip6tables -t mangle -F
# ip6tables -F
# ip6tables -X
```


<a id="orgc8c8062"></a>

## Name cluster

```shell
cat /sys/class/dmi/id/product_uuid
ip link

CLUSTER_IP=192.168.33.10
CLUSTER_NAME=k8s.luislain.com
DATA_DIR=/vagrant_data
CALICO_DIR=${DATA_DIR}/var/calico

hostname
hostnamectl set-hostname ${CLUSTER_NAME}
hostname
echo "${CLUSTER_IP}	${CLUSTER_NAME}" >> /etc/hosts
```


<a id="org71341ea"></a>

## Init cluster

```shell
kubeadm init --control-plane-endpoint=${CLUSTER_NAME} \
	--apiserver-advertise-address=${CLUSTER_IP} \
	--pod-network-cidr 192.168.0.0/16
#	--native.cgroupdriver=systemd
```


<a id="org9a5d641"></a>

## Set-up kubectl

```shell
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> /root/.profile
export KUBECONFIG=/etc/kubernetes/admin.conf
mkdir -p /$HOME/.kube
cp -v $KUBECONFIG $HOME/.kube/config
chown -Rv $(id -u):$(id -g) $HOME/.kube
cp -vf $KUBECONFIG "${DATA_DIR}/config.${CLUSTER_NAME}"
kubectl get pod -A
```


<a id="orgd16c3bb"></a>

## Deploy calico

```shell
mkdir -p $CALICO_DIR && cd $CALICO_DIR
if [ ! -f calico-v1.yaml ]; then
    curl https://docs.projectcalico.org/manifests/custom-resources.yaml -o calico-custom-res.yaml
    curl https://docs.projectcalico.org/manifests/calico.yaml -o calico.yaml
    sed -i.default -e 's/policy\/v1beta1/policy\/v1/' calico.yaml
    mv calico.yaml calico-v1.yaml
fi
kubectl apply -f calico-v1.yaml
cd -
kubectl get pod -A
```


<a id="orgb0f8d17"></a>

## Taint master node

```shell
kubectl taint nodes --all node-role.kubernetes.io/master-
```


<a id="org5e90139"></a>

## Deploy ingress-nginx

```shell
helm upgrade --install ingress-nginx ingress-nginx \
     --repo https://kubernetes.github.io/ingress-nginx \
     --namespace ingress-nginx --create-namespace \
     --set controller.setAsDefaultIngress=true \
     --set controller.service.loadBalancerIP="${CLUSTER_IP}" \
     --set controller.service.externalIPs={"${CLUSTER_IP}"} \
     --set prometheus.create=true
     # --set prometheus.port
kubectl get pod -A
```


<a id="org482fd2f"></a>

## Deploy metrics-server

```shell
helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm upgrade --install metrics-server/metrics-server
kubectl get pod -A
kubectl get svc -A
```


<a id="org6989cd9"></a>

# Test Control plane


<a id="orga8e149c"></a>

## Modify hosts file

```shell
cat hosts.k8s.luislain.com | grep "k8s.luislain.com" >> /etc/hosts
```


<a id="orga4b5506"></a>

## Copy kubeconfig file

```shell
mkdir -p $HOME/.kube && cp -v config.k8s.luislain.com $HOME/.kube
```


<a id="org97b21ae"></a>

## List pods and services

```shell
sh kubectl.k8s.luislain.com.sh get po -A
```


<a id="orgdea118f"></a>

## Join control plane node

```shell
cat controlplane.k8s.luislain.com | grep -B2 "\-\-control-plane"
```


<a id="org478f34d"></a>

## Join worker node

```shell
cat controlplane.k8s.luislain.com | grep -A1 "join"
```
